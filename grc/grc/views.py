""" GRC index module """

from django.shortcuts import render


def index(request):
    """ Index Page """

    return render(request, 'grc/index.html')
