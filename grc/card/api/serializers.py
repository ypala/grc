""" Card Serializer Module """

from rest_framework import serializers
from card.models import Card


class CardSerializer(serializers.ModelSerializer):
    """ Card Serializer Class """

    class Meta:
        model = Card
        fields = [
            'name', 'slug', 'front_face', 'back_face',
            'front_face_img', 'front_face_sound', 'back_face_sound'
            ]
