""" Card Urls Module """

from django.urls import path
from card.api.views import CardListAPIView, CardDetailAPIView

urlpatterns = [
    path('list/<number>', CardListAPIView.as_view(), name='list'),
    path('detail/<slug>', CardDetailAPIView.as_view(), name='detail')
]
