""" Card API Views Module """

import random
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticated
from card.models import Card
from card.api.serializers import CardSerializer


class CardListAPIView(ListAPIView):
    """ Get Card list """

    serializer_class = CardSerializer

    permission_classes = [IsAuthenticated]

    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['name', 'front_face', 'back_face']

    def get_queryset(self):
        number = int(self.kwargs['number'])

        queryset = list(Card.objects.filter(draft=False))
        random.shuffle(queryset)

        return queryset[:number]


class CardDetailAPIView(RetrieveAPIView):
    """ Get Card Detail """

    serializer_class = CardSerializer

    permission_classes = [IsAuthenticated]

    lookup_field = 'slug'

    def get_queryset(self):
        queryset = Card.objects.filter(draft=False)

        return queryset
