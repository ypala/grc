""" Card Model Module """

from django.db import models
from django.utils import timezone
from django.utils.text import slugify


class Card(models.Model):
    """ Card Model Class """

    name = models.CharField(max_length=50)
    slug = models.SlugField(editable=False, unique=True)
    front_face = models.TextField()
    back_face = models.TextField()
    draft = models.BooleanField(default=False)
    front_face_img = models.ImageField(upload_to='card', blank=True)
    front_face_sound = models.FileField(upload_to='card', blank=True)
    back_face_sound = models.FileField(upload_to='card', blank=True)
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    def get_slug(self):
        """ convert name information to slug. """

        slug = slugify(self.name.replace('ı', 'i'))
        unique = slug
        number = 1

        while Card.objects.filter(slug=unique).exists():
            unique = '{}-{}'.format(slug, number)
            number += 1

        return unique


    def save(self, *args, **kwargs):
        if not self.id:
            self.created = timezone.now()

        self.modified = timezone.now()
        self.slug = self.get_slug()

        return super(Card, self).save(*args, **kwargs)
